import type { User } from "@/types/User";
import http from "./http";

function addUser(user: User){
    return http.post('/users/', user) //add
}

function updateUser(user: User){
    return http.patch('/users/'+user.id, user) //update
}

function removeUser(user: User){
    return http.delete('/users/'+user.id) //delete
}

function getUser(id: number){
    return http.get('/users'+id) 
}

function getUsers(){
    return http.get('/users') 
}

export default { addUser, updateUser, removeUser, getUser, getUsers}