import http from "./http"

type ReturnData = {
    celsius: number,
    fahrenheit: number
}

async function convert(celsius: number):Promise<number>{
    const res = await http.post('/temperature/convert', {
        celsius: celsius
      })
      const convertResult = res.data as ReturnData
      return convertResult.fahrenheit
}

export default { convert }