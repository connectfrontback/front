import { ref } from 'vue'
import { defineStore } from 'pinia'
import userService from '@/services/user'
import { useLoadingStore } from './loading'
import type { User } from '@/types/User'

const loadingStore = useLoadingStore()

export const useUserStore = defineStore('user', () => {
  const users = ref<User[]>([])

  async function getUser(id: number) {
    loadingStore.doLoad()
    const res = await userService.getUser(id)
    users.value = res.data
    loadingStore.finish()
  }

  async function getUsers() {
    loadingStore.doLoad()
    const res = await userService.getUsers()
    users.value = res.data
    loadingStore.finish()
  }
  
  async function saveUser(user: User){
    loadingStore.doLoad()
    if (user.id < 0) {
      const res = await userService.addUser(user)
    } else {
      const res = await userService.updateUser(user)
    }
    await getUsers()
    loadingStore.finish()
  }
  
  async function deleteUser(user: User) {
    loadingStore.doLoad()
    const res = await userService.removeUser(user)
    await getUsers()
    loadingStore.finish()
  }

  return { getUser, saveUser, deleteUser, getUsers, users}
})
