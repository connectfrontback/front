import { ref } from 'vue'
import { defineStore } from 'pinia'

export const useLoadingStore = defineStore('loading', () => {
  const isLoading = ref(false)
  const doLoad = () => {
    isLoading.value = true
  }
  const finish = () => {
    isLoading.value = false
  }

  return { isLoading, finish, doLoad }
})
